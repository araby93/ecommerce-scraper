<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\ECommerce\Scraper;

use ITeam\Ecommerce\Scraper\Parsers\AliExpress;
use ITeam\Ecommerce\Scraper\Parsers\Amazon;
use ITeam\Ecommerce\Scraper\Parsers\Carters;
use ITeam\Ecommerce\Scraper\Parsers\ChildrensPlace;
use ITeam\Ecommerce\Scraper\Parsers\Ebay;
use ITeam\Ecommerce\Scraper\Parsers\ElfCosmetics;
use ITeam\Ecommerce\Scraper\Parsers\FinishLine;
use ITeam\Ecommerce\Scraper\Parsers\Forever21;
use ITeam\Ecommerce\Scraper\Parsers\Gap;
use ITeam\Ecommerce\Scraper\Parsers\GapFactory;
use ITeam\Ecommerce\Scraper\Parsers\Guess;
use ITeam\Ecommerce\Scraper\Parsers\Gymboree;
use ITeam\Ecommerce\Scraper\Parsers\NewBalance;
use ITeam\Ecommerce\Scraper\Parsers\NyxCosmetics;
use ITeam\Ecommerce\Scraper\Parsers\RalphLauren;
use ITeam\Ecommerce\Scraper\Parsers\Sephora;
use ITeam\Ecommerce\Scraper\Parsers\SixPM;
use ITeam\Ecommerce\Scraper\Parsers\VictoriasSecret;

/**
 * Class Config
 * @package ITeam\ECommerce\Scraper
 */
class Config
{
    public static $domainParsers = [
        'amazon' => Amazon::class,
        'forever21' => Forever21::class,
        'gapfactory' => GapFactory::class,
        'carters' => Carters::class,
        'ebay' => Ebay::class,
        '6pm' => SixPM::class,
        'gap' => Gap::class,
        'aliexpress' => AliExpress::class,
        'victoriassecret' => VictoriasSecret::class,
        'sephora' => Sephora::class,
        'guessfactory' => Guess::class,
        'gbyguess' => Guess::class,
        'guess' => Guess::class,
        'ralphlauren' => RalphLauren::class,
        'newbalance' => NewBalance::class,
        'childrensplace' => ChildrensPlace::class,
        'finishline' => FinishLine::class,
        'elfcosmetics' => ElfCosmetics::class,
        'nyxcosmetics' => NyxCosmetics::class,
        'gymboree' => Gymboree::class
    ];

    public static $userAgents = [];
    public static $proxies = [];
}
