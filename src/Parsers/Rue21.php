<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Rue21
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Rue21 extends BaseParser
{
    protected $simulateBrowser = true;
    protected $pageJson;
    protected $productData;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->sanitizeString($this->productData['name']);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->sanitizeString($this->crawler->filter('meta[property="og:description"]')->attr('content'));
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->sanitizePrice($this->productData['colorPriceGroups'][0]['maxCurrentPrice']);

        if (empty($price)) {
            $price = $this->getOriginalPrice();
        }
        return $price;
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        return $this->sanitizePrice($this->productData['colorPriceGroups'][0]['maxListPrice']);
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];

        $images[] = $this->sanitizeString($this->crawler->filter('meta[property="og:image"]')->attr('content'));

        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];

        $this->crawler->filter('ul.breadcrumbs li:not(:first-child) a')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->value($node);
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];

        $addedAttributeValues = [];

        foreach ($this->productData['skus'] as $sku) {
            foreach ($sku['variants'] as $key => $variant) {
                $key = ucfirst(str_replace('VariantType', '', $key));
                if (array_key_exists($key, $attributes) === false) {
                    $attributes[$key] = new Attribute($key);
                    $addedAttributeValues[$key] = [];
                }

                if (in_array($variant['displayName'], $addedAttributeValues[$key]) === false) {
                    $attributes[$key]->addValue(
                        new AttributeValue($variant['displayName'], $sku['price'], $sku['price'])
                    );
                    $addedAttributeValues[$key][] = $variant['displayName'];
                }

            }
        }

        return array_values($attributes);
    }

    protected function initData()
    {
        preg_match('/\bMAINdata\.productData = (.*)/m', $this->crawler->html(), $dataJson);
        $this->pageJson = json_decode($dataJson[1], true);
        $this->productData = reset($this->pageJson);
    }
}
