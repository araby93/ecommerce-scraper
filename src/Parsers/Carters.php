<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Carters
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Carters extends BaseParser
{
    protected $useProxy = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('h1.product-title'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value($this->crawler->filter('#collapsible-details-null'), 'html');
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.product-price .price-original > span:first-child'),
            'price'
        );

        if (empty($price)) {
            $price = $this->value(
                $this->crawler->filter('.product-details .product-price .msrp'),
                'price'
            );
        }

        if (empty($price)) {
            $price = $this->getSalePrice();
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        return $this->value($this->crawler->filter('.product-price .price-sale  > span:first-child'), 'price');
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];

        try {
            $images[] = $this->sanitizeString($this->crawler->filter('meta[property="og:image"]')->attr('content'));
        } catch (\Exception $e) {
        }

        foreach ($this->crawler->filter('#thumbnails ul li.thumb img')->images() as $image) {
            $images[] = strtok($image->getUri(), '?');
        }

        if ($this->crawler->filter('#primaryImage > a img')->count()) {
            $images[] = strtok($this->crawler->filter('#primaryImage > a img')->image()->getUri(), '?');
        }

        return array_unique($images);
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];
        $this->crawler->filter('.breadcrumbs .breadcrumbs-item a')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->value($node);
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];
        $this->crawler->filter('.product-detail .product-variations li.attribute span.label')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = str_replace(':', '', $this->value($node));
                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }

                $node->siblings()->filter('ul li a')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $valueLabel = $this->sanitizeString($node->attr('title'));
                        $attributes[$name]->addValue(new AttributeValue($valueLabel));
                    });
            });

        return array_values($attributes);
    }
}
