<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Urbanog
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Urbanog extends BaseParser
{
    protected $simulateBrowser = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('.product_detail_content h1.product-title'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value(
            $this->crawler->filter('.product_detail_content h2.prod-sec1 + p'),
            'html',
            'html'
        );
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.product_detail_content .product-price .market-price'),
            'price'
        );

        if (empty($price)) {
            $price = $this->getSalePrice();
        }


        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.product_detail_content .product-price .cart-price'),
            'price'
        );

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('.product_detail_content #image_list img')
            ->each(function (Crawler $node) use (&$images) {
                $images[] = $this->sanitizeString($node->image()->getUri());
            });
        return array_unique($images);
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return [];
    }
}
