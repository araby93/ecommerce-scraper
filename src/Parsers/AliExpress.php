<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class AliExpress
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class AliExpress extends BaseParser
{
    protected $simulateBrowser = true;
    /**
     * @return string
     */
    public function getName(): string
    {
        $name = $this->value($this->crawler->filter('h1.product-title-text'));

        if (empty($name)) {
            $name = $this->value($this->crawler->filter('h1.detail-description'));
        }

        return $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.product-price .product-price-del .product-price-value'),
            'price'
        );

        if (empty($price)) {
            $price = $this->value(
                $this->crawler->filter('.detail-price-wrap .original-price'),
                'price'
            );
        }

        if (empty($price)) {
            $price = $this->getSalePrice();
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value($this->crawler->filter('.product-price .product-price-current .product-price-value'), 'price');

        if (empty($price)) {
            $price = $this->value(
                $this->crawler->filter('.detail-price-wrap .current-price'),
                'price'
            );
        }

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('.images-view-list li img')
            ->each(function (Crawler $node) use (&$images) {
                $thumbUri = $node->image()->getUri();
                $extension = substr($thumbUri, strrpos($thumbUri, '.'));
                $images[] = $this->sanitizeString(
                    substr($thumbUri, 0, stripos($thumbUri, $extension) + strlen($extension))
                );
            });

        $this->crawler->filter('#detail-carousel amp-img')
            ->each(function (Crawler $node) use (&$images) {
                $thumbUri = $node->attr('src');
                $extension = substr($thumbUri, strrpos($thumbUri, '.'));
                $images[] = $this->sanitizeString(
                    substr($thumbUri, 0, stripos($thumbUri, $extension) + strlen($extension))
                );
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];
        $this->crawler->filter('.breadcrumb a')
            ->each(function (Crawler $node) use (&$categories) {
                $title = $node->attr('title');
                if ($title !== null && $title !== '') {
                    $categories[] = $title;
                }
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];
        $this->crawler->filter('#j-detail-page #j-product-info-sku .p-item-title')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = str_replace(':', '', $this->value($node));
                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }
                $node->siblings()->filter('.p-item-main li img, .p-item-main li span')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $valueLabel = $this->sanitizeString($node->attr('title'));
                        if (empty($valueLabel)) {
                            $valueLabel = $this->value($node);
                        }

                        $attributes[$name]->addValue(new AttributeValue($valueLabel));
                    });
            });

        return array_values($attributes);
    }
}
