<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Pdp\Parser;
use Pdp\PublicSuffixListManager;

/**
 * Class Sephora
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Sephora extends BaseParser
{
    protected $simulateBrowser = false;
    protected $pageJson;
    protected $productData;
    protected $domainParser;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->sanitizeString($this->productData['displayName']);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->sanitizeString($this->productData['longDescription']);
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        if (isset($this->productData['currentSku']['salePrice'])) {
            return $this->getSalePriceFromSkuItem($this->productData['currentSku']);
        }

        return $this->getOriginalPrice();
    }

    protected function getSalePriceFromSkuItem($item)
    {
        if (isset($item['salePrice'])) {
            $price = $this->sanitizePrice($item['salePrice']);
        }
        if (empty($price)) {
            $price = $this->getOriginalPriceFromSkuItem($item);
        }

        return $price;
    }

    protected function getOriginalPriceFromSkuItem($item)
    {
        return $this->sanitizePrice($item['listPrice']);
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        return $this->getOriginalPriceFromSkuItem($this->productData['currentSku']);
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];

        $url = $this->domainParser->parseUrl($this->crawler->getUri());
        $baseUrl = $url->getScheme() . '://' . $url->getHost();

        $bestRes = 0;
        $bestResKey = '';

        foreach ($this->productData['currentSku']['skuImages'] as $size => $imagePath) {
            $res = str_replace(['image', 'altText'], '', $size);
            if ($res > $bestRes) {
                $bestRes = $res;
                $bestResKey = $size;
            }
        }

        $images[] = $baseUrl . $this->productData['currentSku']['skuImages'][$bestResKey];

        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->getChildCategory($this->productData['parentCategory']);
    }

    protected function getChildCategory($category)
    {
        $categories = [];
        $categories[] = $category['displayName'];

        if (isset($category['parentCategory'])) {
            $categories = array_merge($categories, $this->getChildCategory($category['parentCategory']));
        }

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $allSkus = $this->getAllSkus();

        $attributes = [];

        foreach ($allSkus as $sku) {
            if (!array_key_exists($sku['variationType'], $attributes)) {
                $attributes[$sku['variationType']] = new Attribute($sku['variationType']);
            }

            $attributes[$sku['variationType']]->addValue(
                new AttributeValue(
                    $sku['variationValue'],
                    $this->getOriginalPriceFromSkuItem($sku),
                    $this->getSalePriceFromSkuItem($sku)
                )
            );
        }

        return array_values($attributes);
    }

    protected function getAllSkus()
    {
        return array_merge(
            $this->productData['onSaleChildSkus'] ?? [],
            $this->productData['regularChildSkus'] ?? []
        );
    }

    protected function initData()
    {
        $this->pageJson = json_decode($this->crawler->filter('script[id="linkJSON"]')->text(), true);
        $this->productData = array_filter($this->pageJson, function ($item) {
            return array_key_exists('path', $item) && $item['path'] === 'RegularProductTop';
        });

        $this->productData = reset($this->productData);
        $this->productData = $this->productData['props']['currentProduct'];

        $pslManager = new PublicSuffixListManager();
        $this->domainParser = new Parser($pslManager->getList());
        $url = $this->domainParser->parseUrl($this->crawler->getUri());
        parse_str($url->getQuery(), $urlQueryItems);

        if (isset($urlQueryItems['skuId'])) {
            foreach ($this->getAllSkus() as $sku) {
                if ($sku['skuId'] === $urlQueryItems['skuId']) {
                    $this->productData['currentSku'] = $sku;
                }
            }
        }
    }
}
