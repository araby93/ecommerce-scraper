<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Gap
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Gap extends BaseParser
{
    protected $simulateBrowser = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('.product-title'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->sanitizeString($this->crawler->filter('meta[name="description"]')->attr('content'));
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = '';
        $price = $this->value(
            $this->crawler->filter('.buybox-container .product-price__strike'),
            'price'
        );

        if (empty($price)) {
            $price = $this->getOriginalPrice();
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        try {
            return $this->value(
                $this->crawler->filter('.buybox-container .pdp-pricing__selected'),
                'price'
            );
        } catch (\InvalidArgumentException $e) {
            return '';
        }
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('.product-photo .slick-slide a')
            ->each(function (Crawler $node) use (&$images) {
                $images[] = $node->link()->getUri();
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];
        $this->crawler->filter('.product-breadcrumb a')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->value($node);
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];
        $this->crawler->filter(
            '.buybox-container .swatches .swatch-group input, .buybox-container .pillbox-wrapper input'
        )->each(function (Crawler $node) use (&$attributes) {
            $name = ucfirst($node->attr('name'));

            if (strpos($name, '-') !== false) {
                $name = substr($name, 0, strpos($name, '-'));
            }

            if (array_key_exists($name, $attributes) === false) {
                $attributes[$name] = new Attribute($name);
            }
            $swatchText = $node->siblings()->filter('.swatch__text');

            if ($swatchText->count()) {
                $valueLabel = $swatchText->text();
            } else {
                $valueLabel = $node->attr('value');
            }

            $valueLabel = ucwords($valueLabel);

            $attributes[$name]->addValue(new AttributeValue($valueLabel));
        });

        return array_values($attributes);
    }
}
