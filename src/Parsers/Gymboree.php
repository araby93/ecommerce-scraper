<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Gymboree
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Gymboree extends BaseParser
{
    protected $simulateBrowser = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('.pdp-main .product-top-div .product-name'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value(
            $this->crawler->filter('#pdpMain .product-info .longDescription'),
            'html',
            'html'
        );
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.pdp-main .product-price .price-standard'),
            'price'
        );

        if (empty($price)) {
            $price = $this->getSalePrice();
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.pdp-main .product-price .price-sales'),
            'price'
        );

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('#pdpMain #amp-container .nav-container ul.amp-carousel li.amp-slide img')
            ->each(function (Crawler $node) use (&$images) {
                $imageUri = $node->image()->getUri();
                if (strpos($imageUri, '?') !== false) {
                    $imageUri = substr($imageUri, 0, strpos($imageUri, '?'));
                }

                $images[] = $this->sanitizeString($imageUri);
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];

        $this->crawler->filter('.breadcrumb *:not(:first-child) a[href] span')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->sanitizeString($node->text());
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];

        $this->crawler->filter('.product-options .attribute, .product-variations .attribute')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = ucfirst($this->value($node->filter(' .label .h6')));

                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }

                $node->filter('.swatches li img, .swatches li a')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $value = $this->sanitizeString($node->attr('alt'));

                        if (empty($value)) {
                            $value = $this->value($node);
                        }

                        if ($value === '') {
                            return;
                        }
                        $attributes[$name]->addValue(new AttributeValue($value));
                    });
            });

        return array_values($attributes);
    }
}
