<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class NewBalance
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class NewBalance extends BaseParser
{
    protected $simulateBrowser = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->sanitizeString($this->crawler->filter('meta[property="og:title"]')->attr('content'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->sanitizeString($this->crawler->filter('meta[property="og:description"]')->attr('content'));
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.product-detail .product-info .product-pricing .pricenote span'),
            'price'
        );

        if (empty($price)) {
            $price = $this->getSalePrice();
        }


        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.product-detail .product-info .product-pricing .price'),
            'price'
        );

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('.product-gallery .swiper-slide img')
            ->each(function (Crawler $node) use (&$images) {
                $images[] = $this->sanitizeString($node->attr('data-src'));
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];

        $this->crawler->filter('.fullBreadcrumbs .product-categories .c-path')
            ->each(function (Crawler $node) use (&$categories) {
                $categoryName = $this->sanitizeString($node->text());

                if (strtolower($categoryName) === 'home') {
                    return;
                }

                $categories[] = $categoryName;
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];
        $this->crawler->filter('div[class^="variant-selector-container"]')
            ->each(function (Crawler $node) use (&$attributes) {
                $nodeText = $node->filter('div[class^="selected"]')->text();
                $textParts = explode(':', $nodeText);
                if (isset($textParts[0])) {
                    $name = $this->sanitizeString($textParts[0]);
                } else {
                    $name = $nodeText;
                }

                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }

                $node->filter('div[class^="selector-container"] .variant-select.swatches > div')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $value = $this->sanitizeString(str_replace('_', ' ', $node->attr('data-value')));
                        if ($value === '') {
                            return;
                        }
                        $attributes[$name]->addValue(new AttributeValue($value));
                    });
            });

        $this->crawler->filter('.selector-container .selectors .variant-select-wrap')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = $this->value($node->filter('.title'));

                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }

                $node->filter('ul.variant-select li')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $value = $this->sanitizeString($node->attr('data-display-value'));
                        if ($value === '') {
                            return;
                        }
                        $attributes[$name]->addValue(new AttributeValue($value));
                    });
            });

        return array_values($attributes);
    }
}
