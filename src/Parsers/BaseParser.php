<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Class BaseParser
 * @package ITeam\Ecommerce\Scraper\Parsers
 */
abstract class BaseParser implements ParserInterface
{
    /**
     * @var Crawler
     */
    protected $crawler;
    /**
     * @var bool
     */
    protected $simulateBrowser = false;
    protected $useProxy = false;

    /**
     * BaseParser constructor.
     * @param Crawler $crawler
     */
    public function __construct(Crawler $crawler = null)
    {
        if ($crawler === null) {
            $crawler = new Crawler();
        }

        $this->setCrawler($crawler);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return [
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'original_price' => $this->getOriginalPrice(),
            'sale_price' => $this->getSalePrice(),
            'images' => $this->getImages(),
            'categories' => $this->getCategories(),
            'attributes' => $this->getAttributes(),
        ];
    }

    /**
     * @return Crawler
     */
    public function getCrawler(): Crawler
    {
        return $this->crawler;
    }

    /**
     * @param Crawler $crawler
     */
    public function setCrawler(Crawler $crawler): void
    {
        $this->crawler = $crawler;

        if ($this->crawler->count()) {
            $this->initData();
        }
    }

    /**
     * @return bool
     */
    public function isSimulateBrowser(): bool
    {
        return $this->simulateBrowser;
    }

    /**
     * @return bool
     */
    public function isUseProxy(): bool
    {
        return $this->useProxy;
    }

    protected function value(Crawler $node, $sanitizeAs = 'string', $crawlerMethod = 'text', $default = '')
    {
        try {
            $sanitizerPrefix = 'sanitize';

            if ($sanitizeAs !== null) {
                $sanitizer = $sanitizerPrefix . ucfirst($sanitizeAs);
                return $this->$sanitizer($node->$crawlerMethod());
            }

            return $node->$crawlerMethod();
        } catch (\Exception $e) {
            return $default;
        }
    }

    /**
     * @param $string
     * @return string
     */
    protected function sanitizeString($string): string
    {
        return trim($string);
    }

    /**
     * @param $string
     * @return mixed
     */
    protected function sanitizeNumber($string)
    {
        return filter_var(trim($string), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    }

    protected function sanitizePrice($string)
    {
        $price = $this->sanitizeNumber($string);
        if (stripos($price, '-') !== false) {
            $prices = explode('-', $price);
            if (isset($prices[1])) {
                return $prices[1];
            }
        }
        $price = str_replace('+', '', $price);
        return $price;
    }

    /**
     * @param $html
     * @return string
     */
    protected function sanitizeHtml($html): string
    {
        return htmlspecialchars(trim(strip_tags($html)));
    }

    protected function initData()
    {
    }
}
