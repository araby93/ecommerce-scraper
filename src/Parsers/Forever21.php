<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Forever21
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Forever21 extends BaseParser
{
    protected $simulateBrowser = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('h1#h1Title'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        $description = '';
        $this->crawler->filter('div#divDescription section')
            ->each(function (Crawler $node) use (&$description) {
                $description .= $this->value($node, 'html');
            });

        return $description;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value($this->crawler->filter('#ItemPrice .p_sale'), 'price');

        if (empty($price)) {
            $price = $this->getOriginalPrice();
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value($this->crawler->filter('#ItemPrice div.p_price span:first-child'), 'price');

        if (empty($price)) {
            $price = $this->getSalePrice();
        }

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('#pic_container .owl-item:not(.cloned) img')
            ->each(function (Crawler $node) use (&$images) {
                $imageUrl = $node->image()->getUri();
                if (stripos($imageUrl, 'loading') === false && stripos($imageUrl, 'logo') === false) {
                    $images[] = $imageUrl;
                }
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];
        $this->crawler->filter('div.breadcrumb a')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->value($node);
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        $attributes = [
            $this->getColorAttribute(),
            $this->getSizeAttribute()
        ];
        return $attributes;
    }

    /**
     * @return Attribute
     */
    protected function getColorAttribute(): Attribute
    {
        $attribute = new Attribute('color');
        $this->crawler->filter('#ColorSizeArea ul#colorButton li')
            ->each(function (Crawler $node) use (&$attribute) {
                $label = strtoupper($node->filter('img')->attr('alt'));
                $onclickFn = str_replace(['\'', '"', '(', ')'], '', $node->attr('onclick'));
                $onClickParams = explode(',', $onclickFn);
                $attribute->addValue(new AttributeValue($label, $onClickParams[3], $onClickParams[2]));
            });
        return $attribute;
    }

    /**
     * @return Attribute
     */
    protected function getSizeAttribute(): Attribute
    {
        $attribute = new Attribute('size');
        $this->crawler->filter('#ColorSizeArea ul#sizeButton li span')
            ->each(function (Crawler $node) use (&$attribute) {
                $attribute->addValue(new AttributeValue($this->value($node)));
            });
        return $attribute;
    }
}
