<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class SixPM
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class SixPM extends BaseParser
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('#productRecap #overview h1'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value($this->crawler->filter('#productRecap #itemInformation div > div > div'), 'html', 'html');
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value($this->crawler->filter('#productRecap aside div > div > div > span'), 'price');
        if (empty($price)) {
            $price = $this->getOriginalPrice();
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        return $this->value(
            $this->crawler->filter('#productRecap aside div > div > div > span > span > span:not(:first-child)'),
            'price'
        );
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('#productRecap #thumbnailsList li img')
            ->each(function (Crawler $node) use (&$images) {
                $thumbUri = $node->image()->getUri();
                $images[] = preg_replace('/(\.\_)(.*)(\_\.)/m', '.', $thumbUri);
            });

        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];
        $this->crawler->filter('#breadcrumbs a:not(:first-child)')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->value($node);
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];
        $this->crawler->filter('#buyBox form label')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = $this->value($node);
                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }
                $havingOptions = false;
                $node->siblings()->filter('select > option')
                    ->each(function (Crawler $node) use (&$attributes, &$havingOptions, $name) {
                        $value = $this->sanitizeString($node->attr('value'));
                        if ($value === '') {
                            return;
                        }
                        $valueLabel = $this->value($node);
                        $attributes[$name]->addValue(new AttributeValue($valueLabel));
                        $havingOptions = true;
                    });

                if (false === $havingOptions) {
                    $node->siblings()->filter('div')
                        ->each(function (Crawler $node) use (&$attributes, $name) {
                            $valueLabel = $this->value($node);
                            $attributes[$name]->addValue(new AttributeValue($valueLabel));
                        });

                }
            });
        return array_values($attributes);
    }
}
