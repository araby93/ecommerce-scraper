<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Ebay
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Ebay extends BaseParser
{
    protected $simulateBrowser = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        $titleNode = $this->crawler->filterXPath('//h1[@id="itemTitle"]');
        $titleHtml = $titleNode->html();
        $titleParts = explode('</span>', $titleHtml);
        if (isset($titleParts[1])) {
            return $this->sanitizeString($titleParts[1]);
        }
        return $this->value($titleNode);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('#mainContent form #prcIsum'),
            'price'
        );

        if (empty($price)) {
            $price = $this->value(
                $this->crawler->filter('#mainContent form .vi-originalPrice'),
                'price'
            );
        }

        if (empty($price)) {
            $price = $this->getSalePrice();
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value($this->crawler->filter('#mainContent form #prcIsumConv span#convbinPrice'), 'price');

        if (empty($price)) {
            $price = $this->value($this->crawler->filter('#mainContent form #prcIsum'), 'price');
        }

        if (empty($price)) {
            $price = $this->value($this->crawler->filter('#mainContent form #mm-saleDscPrc'), 'price');
        }

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];

        $mainImage = $this->crawler->filter('meta[property="og:image"]')->attr('content');

        $imageSuffix = substr($mainImage, strrpos($mainImage, '/') + 1);

        $this->crawler->filter('#vi_main_img_fs ul li img')
            ->each(function (Crawler $node) use (&$images, $imageSuffix) {
                $thumbUri = $node->image()->getUri();
                $imageUri = substr($thumbUri, 0, strrpos($thumbUri, '/') + 1) . $imageSuffix;
                $images[] = $imageUri;
            });

        return array_unique($images);
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];
        $this->crawler->filter('#vi-VR-brumb-lnkLst ul li[itemprop="itemListElement"] a span')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->value($node);
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];

        $this->crawler->filter('#mainContent form select')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = $this->sanitizeString($node->attr('name'));

                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }

                $node->filter('option')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $value = $this->sanitizeString($node->attr('value'));
                        $valueLabel = $this->value($node);
                        if ($value === '' || $value === '-1') {
                            return;
                        }
                        $attributes[$name]->addValue(new AttributeValue($valueLabel));
                    });
            });

        return array_values($attributes);
    }
}
