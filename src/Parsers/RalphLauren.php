<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class RalphLauren
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class RalphLauren extends BaseParser
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('#product-details h1.product-name'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value(
            $this->crawler->filter('#product-details .product-detailed-information #pdp-details-accordion .product-content-section'),
            'html',
            'html'
        );
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('#product-details #product-content .product-price .lowblack'), 'price'
        );

        if (empty($price)) {
            $price = $this->getSalePrice();
        }


        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('#product-details #product-content .product-price .selected-color-text'), 'price'
        );

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('#product-details #s7viewer-main picture source:first-child')
            ->each(function (Crawler $node) use (&$images) {
                $images[] = $this->sanitizeString($node->attr('srcset'));
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];

        $this->crawler->filter('.breadcrumb a.breadcrumb-element')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->value($node);
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];

        $this->crawler->filter('#product-details #product-content li.attribute')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = $this->sanitizeString(ucfirst(str_replace('attribute', '', $node->attr('class'))));

                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }

                $node->filter('ul.attribute-list li a')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $value = $this->sanitizeString($node->attr('data-selected'));
                        if ($value === '' || $value === '-1') {
                            return;
                        }
                        $attributes[$name]->addValue(new AttributeValue($value));
                    });
            });

        return array_values($attributes);
    }
}
