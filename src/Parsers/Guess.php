<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Guess
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Guess extends BaseParser
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('#singlepro h1.header-big'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value(
            $this->crawler->filter('#singlepro .prodDescSpacer'),
            'html',
            'html'
        );
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $priceHtml = $this->value($this->crawler->filter('#singlepro .price .promo .priceVal'), null, 'html');
        if (stripos($priceHtml, '<span') !== false) {
            $priceHtml = substr($priceHtml, 0, stripos($priceHtml, '<span'));
        }

        $price = $this->sanitizePrice($priceHtml);

        if (empty($price)) {
            $price = $this->getOriginalPrice();
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $priceNode = $this->crawler->filter('#singlepro .price .original .priceVal');

        return $this->value($priceNode, 'price');
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $images[] = $this->sanitizeString($this->crawler->filter('meta[property="og:image"]')->attr('content'));
        $this->crawler->filter('#singlepro .swiper-container .swiper-slide:not(.swiper-slide-duplicate) img')
            ->each(function (Crawler $node) use (&$images) {
                $images[] = $this->sanitizeString($node->image()->getUri());
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];

        preg_match('/\bpageBreadcrumb\"(.*)$/m', $this->crawler->html(), $matches);

        if (isset($matches[1])) {
            $categoriesString = $this->sanitizeString(str_replace(['"', ':', ','], '', $matches[1]));
            $categories = array_slice(explode('|', $categoriesString), 2);
            array_pop($categories);
        }

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];

        foreach (['color', 'size'] as $attribute) {
            $attributes[$attribute] = new Attribute(ucfirst($attribute));

            $this->crawler->filter(
                '.productInformation .'.$attribute.'box li img, .productInformation .'.$attribute.'box li a'
            )->each(function (Crawler $node) use (&$attributes, $attribute) {
                $valueLabel = ucwords($this->sanitizeString($node->attr('alt')));

                if (empty($valueLabel)) {
                    $valueLabel = ucwords($this->sanitizeString($node->text()));
                }

                if (!empty($valueLabel)) {
                    $attributes[$attribute]->addValue(new AttributeValue($valueLabel));
                }
            });
        }


        return array_values($attributes);
    }
}
