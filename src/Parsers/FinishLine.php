<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class FinishLine
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class FinishLine extends BaseParser
{
    protected $useProxy = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->sanitizeString($this->crawler->filter('meta[property="og:title"]')->attr('content'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value(
            $this->crawler->filter('.product-detail-container div[itemprop="description"]'),
            'html',
            'html'
        );
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('#productPrices .productPrice .wasPrice, #alternatePrices .wasPrice, #alternatePrices .wasSeePrice'),
            'price'
        );

        if (empty($price)) {
            $price = $this->getSalePrice();
        }


        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('#productPrices .productPrice .nowPrice, #alternatePrices .nowPrice, #alternatePrices .productPrice .fullPrice'),
            'price'
        );

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('#productImageLayout .pdp-image img')
            ->each(function (Crawler $node) use (&$images) {
                $images[] = $this->sanitizeString($node->image()->getUri());
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];

        $this->crawler->filter('#breadcrumbs ul li:not(:first-child) a span')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->sanitizeString($node->text());
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [
            'Color' => new Attribute('Color'),
            'Size' => new Attribute('Size')
        ];
        $this->crawler->filter('#alternateColors img')
            ->each(function (Crawler $node) use (&$attributes) {
                $value = $this->sanitizeString($node->attr('alt'));

                if ($value === '') {
                    return;
                }

                $attributes['Color']->addValue(new AttributeValue($value));
            });

        $this->crawler->filter('#productSizes button')
            ->each(function (Crawler $node) use (&$attributes) {
                $value = $this->value($node);

                if ($value === '') {
                    return;
                }

                $attributes['Size']->addValue(new AttributeValue($value));
            });

        return array_values($attributes);
    }
}
