<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Shoes
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Shoes extends BaseParser
{
    protected $simulateBrowser = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->sanitizeString($this->crawler->filter('meta[property="og:title"]')->attr('content'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->sanitizeString($this->crawler->filter('meta[property="og:description"]')->attr('content'));
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.show_product .main_content .cart .regular_price .strikethrough'),
            'price'
        );

        if (empty($price)) {
            $price = $this->getSalePrice();
        }


        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.show_product .main_content .cart .price'),
            'price'
        );

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('#full_product_info .quick_view_body .visual .thumbs img')
            ->each(function (Crawler $node) use (&$images) {
                $imgSrc = $this->sanitizeString($node->attr('src'));
                if (!empty($imgSrc)) {
                    $images[] = str_replace('dt', 'hd', $this->sanitizeString($node->image()->getUri()));
                }
            });

        if (count($images) === 0) {
            $images[] = $this->sanitizeString($this->crawler->filter('meta[property="og:image"]')->attr('content'));
        }

        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];

        $this->crawler->filter('#full_product_info .details .selectors .selBox')
            ->each(function (Crawler $node) use (&$attributes) {
                $nodeText = $this->value($node->filter('legend'));
                $textParts = explode(':', $nodeText);
                if (isset($textParts[0])) {
                    $name = $this->sanitizeString($textParts[0]);
                } else {
                    $name = $nodeText;
                }

                if (empty($name)) {
                    return;
                }

                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }

                $node->filter('.prodOptions a')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $value = $this->sanitizeString($node->attr('data-color-name'));

                        if (empty($value)) {
                            $value = $this->sanitizeString($node->text());
                        }

                        if ($value === '') {
                            return;
                        }

                        $price = $this->value($node->filter('span.color_price'), 'price', 'text', 0);

                        $attributes[$name]->addValue(new AttributeValue($value, $price, $price));
                    });
            });

        return array_values($attributes);
    }
}
