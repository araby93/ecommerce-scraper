<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Etsy
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class Etsy extends BaseParser
{
    protected $simulateBrowser = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('#listing-page-cart h1'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value(
            $this->crawler->filter('#description-text .max-height-text-container .prose'),
            'html',
            'html'
        );
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('#listing-page-cart *[data-buy-box-region*="price"] p > span:first-child'),
            'price'
        );

        return $price;
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('#listing-page-cart *[data-buy-box-region*="price"] p > s'),
            'price'
        );

        if (empty($price)) {
            $price = $this->getSalePrice();
        }


        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('.listing-page-image-carousel-component img')
            ->each(function (Crawler $node) use (&$images) {
                $images[] = $this->sanitizeString($node->image()->getUri());
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];

        preg_match('/\"\bcategory\"(.*)$/m', $this->crawler->html(), $matches);

        if (isset($matches[1])) {
            $categoriesString = $this->sanitizeString(str_replace(['"', ':', ','], '', $matches[1]));
            $categories = explode('&lt;', $categoriesString);
            $categories =  array_map(function ($category) {
                return $this->sanitizeString($category);
            }, $categories);
        }

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];

        $this->crawler->filter('#listing-page-cart #variations > div')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = $this->value($node->filter('label'));

                if (strtolower($name) === 'quantity') {
                    return;
                }

                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }

                $node->filter('select option')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $value = $this->sanitizeString($node->attr('value'));
                        $valueLabel = $this->value($node);
                        if ($value === '' || $value === '-1') {
                            return;
                        }
                        $attributes[$name]->addValue(new AttributeValue($valueLabel));
                    });
            });

        return array_values($attributes);
    }
}
