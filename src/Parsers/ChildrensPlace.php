<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ChildrensPlace
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class ChildrensPlace extends BaseParser
{
    protected $simulateBrowser = true;
    protected $useProxy = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('.product-details-header-container .product-title'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value(
            $this->crawler->filter('.product-description-list .introduction-text'),
            'html',
            'html'
        );
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.outfiting-section-container .original-price, .product-details-container .original-price'),
            'price'
        );

        if (empty($price)) {
            $price = $this->getSalePrice();
        }


        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('.outfiting-section-container .actual-price, .product-details-container .actual-price'),
            'price'
        );

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('.product-images-container .preview-image-item img')
            ->each(function (Crawler $node) use (&$images) {
                $images[] = str_replace('products/125', 'products/500', $this->sanitizeString($node->image()->getUri()));
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];
        $this->crawler->filter('.product-details-form-content fieldset > div:not(.select-common) ')
            ->each(function (Crawler $node) use (&$attributes) {
                $nodeText = $this->value($node->filter('span[class$="-title"]'));
                $name = $this->sanitizeString(ucwords(str_replace(['select a', ':'], '', strtolower($nodeText))));

                if (empty($name)) {
                    return;
                }

                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }

                $node->filter('.input-radio-title > span')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        if (stripos($node->attr('class'), '-container')) {
                            $value = $this->sanitizeString($node->attr('title'));
                        } else {
                            $value = $this->sanitizeString($node->text());
                        }

                        if ($value === '') {
                            return;
                        }
                        $attributes[$name]->addValue(new AttributeValue($value));
                    });
            });

        return array_values($attributes);
    }
}
