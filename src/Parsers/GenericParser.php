<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use Pdp\Parser;
use Pdp\PublicSuffixListManager;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class GenericParser
 * @package ITeam\Ecommerce\Scraper\Parsers
 */
class GenericParser extends BaseParser
{
    /**
     * @var bool
     */
    protected $simulateBrowser = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        $metaSelectors = [
            'meta[property="og:title"]',
            'meta[property="twitter:title"]',
            'meta[property="name"]',
        ];

        $selectors = [
            'h1',
            '#title',
            '#product-name, .product-name',
            '#product-title, .product-title',
            '*[itemprop="headline"]',
            '*[itemprop="name"]'
        ];

        $name = $this->processMetaSelectors($metaSelectors);

        if (empty($name)) {
            $name = $this->processSelectors($selectors);
        }

        return $name;
    }

    /**
     * @param $metaSelectors
     * @return string
     */
    protected function processMetaSelectors($metaSelectors): string
    {
        $value = '';

        foreach ($metaSelectors as $metaSelector) {
            if (empty($value)) {
                try {
                    $value = $this->sanitizeString($this->crawler->filter($metaSelector)->attr('content'));
                } catch (\Exception $e) {
                }
            } else {
                break;
            }
        }
        return $value;
    }

    /**
     * @param $selectors
     * @param string $sanitizeAs
     * @param string $crawlerMethod
     * @return string
     */
    protected function processSelectors($selectors, $sanitizeAs = 'string', $crawlerMethod = 'text'): string
    {
        $value = '';

        foreach ($selectors as $selector) {
            if (empty($value)) {
                $value = $this->value($this->crawler->filter($selector), $sanitizeAs, $crawlerMethod);
            } else {
                break;
            }
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        $metaSelectors = [
            'meta[property="og:description"]',
            'meta[property="twitter:description"]',
            'meta[property="description"]',
            'meta[name="Description"]',
            'meta[name="description"]'
        ];

        $selectors = [
            '*[itemprop="description"]',
            '*[class*="longDescription"]',
            '*[id*="longDescription"]'
        ];

        $description = $this->processMetaSelectors($metaSelectors);

        if (empty($description)) {
            $description = $this->processSelectors($selectors, 'html', 'html');
        }

        return $description;
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $selectors = [
            '*[class*="regularPrice"]',
            '*[class*="regular-price"]',
        ];

        $price = $this->processSelectors($selectors, 'price');

        if (empty($price)) {
            $price = $this->getSalePrice();
        } elseif (substr_count($price, '.') > 1) {
            $priceParts = explode('.', $price);
            $price = $priceParts[0];
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $selectors = [
            '*[class*="wrapper"] *[class*="price"]',
            '*[class*="sale-price"]',
            '*[class*="salePrice"]',
            '*[class*="main-price"]',
            '*[class*="product-price"]',
            '*[class*="cart-price"]',
            '*[class*="selling-price"]',
            '*[class*="currentPrice"]',
            '*[class*="current-price"]',
            'span[class*="price"]',
            'span[class*="prc"]',
            'span[class*="discount"]',
            '*[class*="price"]',
            '*[class*="prc"]',
            '*[class*="discount"]',
        ];

        $price = $this->processSelectors($selectors, 'price');

        if (substr_count($price, '.') > 1) {
            $priceParts = explode('.', $price);
            $price = $priceParts[0];
        }

        return $price;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getImages(): array
    {
        $images = [];

        $metaSelectors = [
            'meta[property="og:image"]',
            'meta[property="twitter:image:src"]'
        ];

        $selectors = [
            'img[itemprop="image"]',
            'img[class*="heroimage"]',
            'img[class*="main-image"]',
            'img[class*="mainImage"]', '
            *[class*="image"] img',
            '*[class*="carousel"] img',
            '*[class*="slide"] img',
            'img[class*="thumb"]',
        ];

        $images[] = $this->processMetaSelectors($metaSelectors);


        $this->crawler->filter(implode(',', $selectors))
            ->each(function (Crawler $node) use (&$images) {
                try {
                    $imageUri = $node->image()->getUri();
                    if (!empty($imageUri)) {
                        $images[] = $node->image()->getUri();
                    }
                } catch (\Exception $e) {
                }
            });

        $pslManager = new PublicSuffixListManager();
        $parser = new Parser($pslManager->getList());
        $websiteUrl = $parser->parseUrl($this->crawler->getUri());

        $images = array_map(function ($image) use ($websiteUrl) {
            if (strpos($image, '//') === 0) {
                return $websiteUrl->getScheme() . ':' . $image;
            }

            if (strpos($image, '/') === 0) {
                return $websiteUrl->getScheme() . '://' . $websiteUrl->getHost() . $image;
            }

            return $image;
        }, $images);

        return array_unique(array_values(array_filter($images)));
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $selectors = [
            '*[class*="breadcrumb"] a',
            '*[id*="breadcrumb"] a',
        ];

        $categories = [];

        $this->crawler->filter(implode(',', $selectors))
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->value($node);
            });

        return array_unique(array_values(array_filter($categories)));
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return [];
    }
}
