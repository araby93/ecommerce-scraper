<?php
/**
 * @author     Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use Symfony\Component\DomCrawler\Crawler;

interface ParserInterface
{
    public function getName(): string;
    public function getDescription(): string;
    public function getSalePrice(): string;
    public function getOriginalPrice(): string;
    public function getImages(): array;
    public function getData(): array;
    public function getCategories(): array;
    public function getAttributes(): array;
    public function isSimulateBrowser(): bool;
    public function getCrawler(): Crawler;
    public function setCrawler(Crawler $crawler): void;
    public function isUseProxy(): bool;
}
