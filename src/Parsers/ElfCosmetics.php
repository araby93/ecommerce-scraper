<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ElfCosmetics
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class ElfCosmetics extends BaseParser
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('#pdpMain .product-content-header .product-name'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value(
            $this->crawler->filter('#pdpMain .product-info .tab-content[itemprop="description"]'),
            'html',
            'html'
        );
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('#pdpMain span[itemprop="price"]'),
            'price'
        );

        if (empty($price)) {
            $price = $this->getSalePrice();
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $price = $this->value(
            $this->crawler->filter('#pdpMain span[itemprop="price"]'),
            'price'
        );

        return $price;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('#pdpMain #thumbnails li img.productthumbnail')
            ->each(function (Crawler $node) use (&$images) {
                $imageUri = $node->image()->getUri();
                if (strpos($imageUri, '?') !== false) {
                    $imageUri = substr($imageUri, 0, strpos($imageUri, '?'));
                }

                $images[] = $this->sanitizeString($imageUri);
            });
        return $images;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];

        $this->crawler->filter('.breadcrumb li:not(:first-child) a[href] span')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->sanitizeString($node->text());
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];

        $this->crawler->filter('#pdpMain .product-variations .attribute')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = $this->value($node->filter('.label'), 'string', 'html');
                if (stripos($name, '<span') !== false) {
                    $name = $this->sanitizeString(substr($name, 0, stripos($name, '<span')));
                }

                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }

                $node->filter('ul.swatch-list li img.productthumbnail')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $value = $this->sanitizeString($node->attr('alt'));
                        if ($value === '') {
                            return;
                        }
                        $attributes[$name]->addValue(new AttributeValue($value));
                    });
            });

        return array_values($attributes);
    }
}
