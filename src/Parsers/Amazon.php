<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Symfony\Component\DomCrawler\Crawler;

class Amazon extends BaseParser
{
    protected $useProxy = true;

    public function getName(): string
    {
        return $this->value(
            $this->crawler->filterXPath('//h1[@id="title"]//span')
        );
    }

    public function getDescription(): string
    {
        return $this->value($this->crawler->filter('#productDescription'), 'html');
    }

    public function getSalePrice(): string
    {
        $price = $this->value(
            $this->crawler->filterXPath(
                '//span[contains(@id,"ourprice") or contains(@id,"saleprice") or contains(@id,"_dealprice")]'
            ),
            'price'
        );

        if (empty($price)) {
            $price = $this->value(
                $this->crawler->filter(
                    '#buybox span[class*="price"]'
                ),
                'price'
            );
        }


        return $price;
    }

    public function getOriginalPrice(): string
    {
        $price = $this->value(
            $this->crawler->filterXPath(
                '//td[contains(text(),"List Price:") or contains(text(),"M.R.P") or contains(text(),"Price")]'
                . '/following-sibling::td/span'
            ),
            'price'
        );
        if (empty($price)) {
            $price = $this->value($this->crawler->filter('#toggleBuyBox .a-color-price'), 'price');
        }

        if (empty($price)) {
            $price = $this->value($this->crawler->filter('#buyBoxInner span.a-text-strike'), 'price');
        }

        return $price;
    }

    public function getImages(): array
    {
        $images = [];
        $this->crawler->filter('div#altImages li[class*="item"] img, #imageBlockThumbs img')
            ->each(function ($node) use (&$images) {
                $thumbUri = $node->image()->getUri();
                $images[] = preg_replace('/(\.\_)(.*)(\_\.)/m', '.', $thumbUri);
            });

        return $images;
    }

    public function getCategories(): array
    {
        $categories = [];
        $this->crawler->filterXPath('//div[@id="wayfinding-breadcrumbs_container"]//li//a')
            ->each(function ($node) use (&$categories) {
                $categories[] = $this->value($node);
            });

        return $categories;
    }

    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];
        $this->crawler->filter('#twister div[id^="variation"]')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = str_replace(':', '', $this->value($node->filter('.a-form-label')));
                $attributes[$name] = new Attribute($name);

                $salePrice = 0;
                $originalPrice = 0;

                $selector = '.selection';
                if ($node->filter($selector)->count()) {
                    $selectedValue = $this->value($node->filter($selector));
                }

                $selector = '.a-dropdown-prompt';
                if ($node->filter($selector)->count()) {
                    $selectedValue = $this->value($node->filter($selector));
                }

                $swatchSelector = '.twisterSwatchWrapper';
                $labelSelector = $swatchSelector . ' .twisterTextDiv';
                if ($node->filter($swatchSelector)->count()) {
                    $selectedValue = $this->value($node->filter($labelSelector));
                    $priceSelector = $swatchSelector . ' .a-color-price';

                    if ($node->filter($priceSelector)->count()) {
                        $originalPrice = $salePrice = $this->value($node->filter($priceSelector), 'price');
                    }
                }

                if (isset($selectedValue)) {
                    $attributes[$name]->addValue(new AttributeValue($selectedValue, $originalPrice, $salePrice));
                }
            });


        $this->crawler->filter('#twister span[id^="declarative"] table tr')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = 'Formats';
                if (!isset($attributes[$name])) {
                    $attributes[$name] = new Attribute($name);
                }

                $nameSelector = '.dp-title-col .title-text span:first-child';

                if ($node->filter($nameSelector)->count()) {
                    $valueLabel = $this->value($node->filter($nameSelector));

                    $price = $this->value($node->filter('.dp-price-col span'), 'price', 'text', 0);

                    $attributes[$name]->addValue(new AttributeValue($valueLabel, $price, $price));
                }


            });
        return $attributes;
    }
}
