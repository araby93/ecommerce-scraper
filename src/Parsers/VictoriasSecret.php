<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Parsers;

use ITeam\Ecommerce\Scraper\Tokens\Attribute;
use ITeam\Ecommerce\Scraper\Tokens\AttributeValue;
use Pdp\Parser;
use Pdp\PublicSuffixListManager;
use Pdp\Uri\Url;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class VictoriasSecret
 * @package ITeam\ECommerce\Scraper\Parsers
 */
class VictoriasSecret extends BaseParser
{
    protected $simulateBrowser = true;
    protected $useProxy = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->value($this->crawler->filter('#breadcrumbs ul li:last-child a'));
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->value($this->crawler->filter('#content .product .pdp-info .long-description'), 'html', 'html');
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        $isDiscount = $this->crawler->filter('#content .product .price-as-shown del')->count() > 0;
        $selector = '#content .product .price-as-shown';

        if ($isDiscount) {
            $selector .= ' div';
        }

        $price = $this->value($this->crawler->filter($selector), 'price');

        if (empty($price)) {
            $price = $this->getOriginalPrice();
        }

        return $price;
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        $isDiscount = $this->crawler->filter('#content .product .price-as-shown del')->count() > 0;
        $selector = '#content .product .price-as-shown';

        if ($isDiscount) {
            $selector .= ' del';
        }

        return $this->value($this->crawler->filter($selector), 'price');
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        $images = [];

        $mainImage = $this->crawler->filter('meta[property="og:image"]')->attr('content');
        $pslManager = new PublicSuffixListManager();
        $parser = new Parser($pslManager->getList());
        $mainImageUrl = $parser->parseUrl($mainImage);
        $urlPathParts = explode('/', $mainImageUrl->getPath());
        $highRes = $urlPathParts[2];

        $this->crawler->filter('#content .product .product-image-group #alt-images img')
            ->each(function (Crawler $node) use (&$images, $parser, $mainImageUrl, $highRes) {
                $thumbUri = $parser->parseUrl($node->attr('src'));
                $urlPathParts = explode('/', $thumbUri->getPath());
                $urlPathParts[2] = $highRes;
                $path = implode('/', $urlPathParts);
                $imageUri = new Url(
                    $mainImageUrl->getScheme(),
                    $mainImageUrl->getUser(),
                    $mainImageUrl->getPass(),
                    $mainImageUrl->getHost(),
                    $mainImageUrl->getPort(),
                    $path,
                    $mainImageUrl->getQuery(),
                    $mainImageUrl->getFragment()
                );
                $images[] = (string)$imageUri;
            });

        return array_unique($images);
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];
        $this->crawler->filter('#breadcrumbs ul li:not(:first-child) a')
            ->each(function (Crawler $node) use (&$categories) {
                $categories[] = $this->value($node);
            });

        return $categories;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        /** @var Attribute[] $attributes */
        $attributes = [];
        $this->crawler->filter('#content form > .product .pdp-info fieldset section[data-selector-wrapper]')
            ->each(function (Crawler $node) use (&$attributes) {
                $name = $this->value($node->filter('div em'));
                if (array_key_exists($name, $attributes) === false) {
                    $attributes[$name] = new Attribute($name);
                }
                $node->filter('div a img')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $valueLabel = $this->sanitizeString($node->attr('alt'));
                        if (!empty($valueLabel)) {
                            $attributes[$name]->addValue(new AttributeValue($valueLabel));
                        }
                    });

                $node->filterXPath('//div//a[not(.//img)]')
                    ->each(function (Crawler $node) use (&$attributes, $name) {
                        $valueLabel = $this->sanitizeString($node->attr('data-value'));
                        if (!empty($valueLabel)) {
                            $attributes[$name]->addValue(new AttributeValue($valueLabel));
                        }
                    });
            });
        return array_values($attributes);
    }
}
