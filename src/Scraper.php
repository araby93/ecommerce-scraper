<?php

namespace ITeam\Ecommerce\Scraper;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Goutte\Client;
use GuzzleHttp\RequestOptions;
use ITeam\Ecommerce\Scraper\Parsers\ParserInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */
class Scraper implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var RemoteWebDriver
     */
    protected $seleniumDriver;

    /**
     * @var string
     */
    protected $seleniumServerUrl;

    /**
     * @var DomainParserFactory
     */
    protected $parserFactory;

    /**
     * @var ParserInterface
     */
    protected $parser;

    /**
     * Scraper constructor.
     * @param LoggerInterface $logger
     * @param string $seleniumServerUrl
     */
    public function __construct(LoggerInterface $logger, $seleniumServerUrl = 'http://localhost:4444/wd/hub')
    {
        set_time_limit(0);

        $this->logger = $logger;
        $this->client = new Client();
        $this->seleniumServerUrl = $seleniumServerUrl;
        $this->parserFactory = new DomainParserFactory($this->logger);
    }

    /**
     * @param string $url
     * @return ParserInterface
     * @throws \Exception
     */
    public function getUrlParser(string $url): ParserInterface
    {
        $this->parser = $this->parserFactory->create(Utils::parseDomain($url));
        return $this->parser;
    }

    /**
     * @param string $url
     * @param int $timeout
     * @return Parsers\ParserInterface
     * @throws \Exception
     */
    public function scrape(string $url, int $timeout = 30): ?Parsers\ParserInterface
    {
        try {
            $this->logger->debug('Started Parsing', ['URL' => $url]);

            $this->parser = $this->getUrlParser($url);
            if ($this->parser->isSimulateBrowser()) {
                $this->logger->debug('Starting Selenium', ['Server URL' => $this->seleniumServerUrl]);
                $this->initSelenium($this->seleniumServerUrl, $timeout, $this->parser->isUseProxy());
                $this->seleniumDriver->get($url);
                $crawler = new Crawler(null, $url);
                $crawler->addContent($this->seleniumDriver->getPageSource());
            } else {
                $this->initGuzzle($this->parser->isUseProxy());
                $crawler = $this->client->request('GET', $url, ['timeout' => $timeout, 'connect_timeout' => $timeout]);
            }

            $this->parser->setCrawler($crawler);

            $this->logger->debug('Scraped Data', ['Data' => $this->parser->getData()]);

            return $this->parser;
        } catch (\Exception $e) {
            $this->logger->error($e, ['URL' => $url]);
            throw $e;
        }
    }

    /**
     * @param $seleniumServerUrl
     * @param bool $useProxy
     */
    protected function initSelenium($seleniumServerUrl, $timeout, $useProxy = false): void
    {
        $capabilities = DesiredCapabilities::chrome();
        $options = new ChromeOptions();

        if ($useProxy && ($proxy = Utils::getRandomProxy()) !== null) {
            $capabilities->setCapability(WebDriverCapabilityType::PROXY, [
                'proxyType' => 'manual',
                'httpProxy' => $proxy,
                'sslProxy' => $proxy,
            ]);
        }

        $options->addArguments([
            '--headless',
            '--user-agent=' . Utils::getRandomUserAgent(),
            '--whitelisted-ips=\'\'',
//            '--proxy-server=' . Utils::getRandomProxy()
        ]);
        $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
        $this->seleniumDriver = RemoteWebDriver::create(
            $seleniumServerUrl,
            $capabilities,
            $timeout * 1000,
            $timeout * 1000
        );
    }

    /**
     * @param bool $useProxy
     */
    protected function initGuzzle($useProxy = false): void
    {
        $clientOptions = [];

        if ($useProxy && ($proxy = Utils::getRandomProxy()) !== null) {
            $clientOptions[RequestOptions::PROXY] = $proxy;
        }

        foreach (Utils::getHeaders() as $key => $value) {
            $this->client->setHeader($key, $value);
        }

        $guzzle = new \GuzzleHttp\Client($clientOptions);
        $this->client->setClient($guzzle);
    }

    /**
     * @param string $domainName
     * @param string $parserClass
     */
    public function addParser(string $domainName, string $parserClass): void
    {
        Config::$domainParsers[$domainName] = $parserClass;
    }

    /**
     * @return array
     */
    public function getParsers(): array
    {
        return Config::$domainParsers;
    }

    /**
     * @param string|string[] $agents
     */
    public function addUserAgents($agents): void
    {
        if (is_string($agents)) {
            Config::$userAgents[] = $agents;
        } elseif (is_array($agents)) {
            Config::$userAgents = array_unique(array_merge(Config::$userAgents, $agents));
        }
    }

    /**
     * @return array
     */
    public function getUserAgents(): array
    {
        return Config::$userAgents;
    }

    /**
     * @param string|string[] $proxies
     */
    public function addProxies($proxies): void
    {
        if (is_string($proxies)) {
            Config::$proxies[] = $proxies;
        } elseif (is_array($proxies)) {
            Config::$proxies = array_unique(array_merge(Config::$proxies, $proxies));
        }
    }
}
