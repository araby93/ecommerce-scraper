<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper;

use Pdp\Parser;
use Pdp\PublicSuffixListManager;

/**
 * Class Utils
 * @package ITeam\Ecommerce\Scraper
 */
class Utils
{
    /**
     * @param $uri
     * @return string
     * @throws \Exception
     */
    public static function parseDomain($uri): string
    {
        $pslManager = new PublicSuffixListManager();
        $parser = new Parser($pslManager->getList());
        $host = $parser->parseUrl($uri)->getHost();
        return substr(
            $host->getRegistrableDomain(),
            0,
            strlen($host->getRegistrableDomain()) - 1
        );
    }

    public static function getHeaders(): array
    {
        return [
            'User-Agent' => self::getRandomUserAgent(),
            'Connection' => 'keep-alive',
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept' => 'text/plain, */*',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Accept-Language' => 'en-US,en;q=0.8',
        ];
    }

    /**
     * @return string
     */
    public static function getRandomUserAgent(): string
    {
        if (empty(Config::$userAgents)) {
            self::loadUserAgents();
        }

        if (empty(Config::$userAgents)) {
            return null;
        }

        try {
            $randomIndex = random_int(0, count(Config::$userAgents) - 1);
        } catch (\Exception $e) {
            $randomIndex = 0;
        }

        return Config::$userAgents[$randomIndex];
    }

    protected static function loadUserAgents(): void
    {
        Config::$userAgents = file(__DIR__ . '/data/user_agents.txt', FILE_IGNORE_NEW_LINES);
    }

    /**
     * @return string|null
     */
    public static function getRandomProxy()
    {
        if (empty(Config::$proxies)) {
            return null;
        }

        try {
            $randomIndex = random_int(0, count(Config::$proxies) - 1);
        } catch (\Exception $e) {
            $randomIndex = 0;
        }

        return Config::$proxies[$randomIndex];
    }
}
