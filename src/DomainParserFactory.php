<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper;

use ITeam\Ecommerce\Scraper\Exceptions\ParserNotFoundException;
use ITeam\Ecommerce\Scraper\Parsers\GenericParser;
use ITeam\Ecommerce\Scraper\Parsers\ParserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;

class DomainParserFactory
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $domainName
     * @return ParserInterface
     */
    public function create(string $domainName)
    {
        $parserName = GenericParser::class;

        try {
            if (isset(Config::$domainParsers[$domainName])) {
                $suggestedParserName = Config::$domainParsers[$domainName];
            } else {
                $className = ucfirst(str_replace('-', '', strtolower($domainName)));
                $fullClassName = (new \ReflectionClass(ParserInterface::class))->getNamespaceName()
                    . '\\'
                    . $className;
                $suggestedParserName = $fullClassName;
            }

            if (!class_exists($suggestedParserName)) {
                throw new ParserNotFoundException("Parser class \"{$suggestedParserName}\" not found");
            }
            $parserName = $suggestedParserName;
        } catch (ParserNotFoundException | \ReflectionException $e) {
            $this->logger->warning($e->getMessage());
            $this->logger->info('Generic parser will be used instead');
        }

        return new $parserName();
    }
}
