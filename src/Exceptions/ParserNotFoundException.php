<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Exceptions;

class ParserNotFoundException extends \Exception
{

}
