<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Tokens;

/**
 * Class AttributeValue
 * @package ITeam\Ecommerce\Scraper\Tokens
 */
class AttributeValue implements \JsonSerializable
{
    /**
     * @var string
     */
    protected $label;
    /**
     * @var string
     */
    protected $originalPrice;
    /**
     * @var string
     */
    protected $salePrice;

    /**
     * AttributeValue constructor.
     * @param $label
     * @param int $originalPrice
     * @param int $salePrice
     */
    public function __construct($label, $originalPrice = 0, $salePrice = 0)
    {
        $this->setLabel($label);
        $this->setOriginalPrice($originalPrice);
        $this->setSalePrice($salePrice);
    }


    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getOriginalPrice(): string
    {
        return $this->originalPrice;
    }

    /**
     * @param string $originalPrice
     */
    public function setOriginalPrice(string $originalPrice): void
    {
        $this->originalPrice = $originalPrice;
    }

    /**
     * @return string
     */
    public function getSalePrice(): string
    {
        return $this->salePrice;
    }

    /**
     * @param string $salePrice
     */
    public function setSalePrice(string $salePrice): void
    {
        $this->salePrice = $salePrice;
    }

    public function jsonSerialize()
    {
        return [
            'label' => $this->label,
            'originalPrice' => $this->originalPrice,
            'salePrice' => $this->salePrice
        ];
    }
}
