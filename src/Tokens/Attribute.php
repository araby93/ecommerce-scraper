<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Tokens;

/**
 * Class Attribute
 * @package ITeam\Ecommerce\Scraper\Tokens
 */
class Attribute implements \JsonSerializable
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var AttributeValue[]
     */
    protected $values;

    /**
     * Attribute constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->setName($name);
        $this->values = [];
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return AttributeValue[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @param AttributeValue[] $values
     */
    public function setValues(array $values): void
    {
        $this->values = $values;
    }

    /**
     * @param AttributeValue $value
     * @return $this
     */
    public function addValue(AttributeValue $value): self
    {
        $this->values[] = $value;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'values' => $this->values
        ];
    }
}
