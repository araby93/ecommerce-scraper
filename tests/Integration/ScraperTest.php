<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Tests\Integration;

use ITeam\Ecommerce\Scraper\Scraper;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

class ScraperTest extends TestCase
{
    /** @var Scraper */
    private $scraper;

    /**
     * @param $url
     * @param $expectedData
     * @return array
     * @throws \Exception
     * @dataProvider dataProvider
     */
    public function testScrape($url, $expectedData): array
    {
        $parser = $this->scraper->scrape($url);
        $data = $parser->getData();

        print_r($data);

        $this->assertEquals($expectedData['name'], $data['name'], 'Product Name');

        if (isset($expectedData['description_prefix'])) {
            $this->assertStringStartsWith(
                $expectedData['description_prefix'],
                $data['description'],
                'Description Prefix'
            );
        }

        if (isset($expectedData['description_suffix'])) {
            $this->assertStringEndsWith(
                $expectedData['description_suffix'],
                $data['description'],
                'Description Suffix'
            );
        }

        $this->assertEquals($expectedData['original_price'], $data['original_price'], 'Original Price');
        $this->assertEquals($expectedData['sale_price'], $data['sale_price'], 'Sale Price');
        $this->assertCount($expectedData['categories_count'], $data['categories'], 'Categories Count');
        $this->assertCount($expectedData['images_count'], $data['images'], 'Images Count');
        $this->assertCount($expectedData['attributes_count'], $data['attributes'], 'Attributes Count');

        return $data;
    }

    public function dataProvider(): array
    {
        return [
            'amazon-1' => [
                'https://amazon.com/Hanes-Pullover-EcoSmart-Fleece-Hoodie/dp/B072LZTRVC/ref=sr_1_2?s=fashion-mens-intl-ship&ie=UTF8&qid=1548810228&sr=1-2&th=1&psc=1',
                [
                    'name' => 'Hanes Men\'s Pullover EcoSmart Fleece Hooded Sweatshirt',
                    'original_price' => '31.95',
                    'sale_price' => '16.46',
                    'categories_count' => 5,
                    'images_count' => 4,
                    'attributes_count' => 2
                ]
            ],
            'aliexpress-1' => [
                'https://www.aliexpress.com/item/Men-s-T-Shirt-2017-Summer-Slim-Fitness-Hooded-Short-Sleeved-Tees-Male-Camisa-Masculina-Sportswer/32800215055.html?spm=2114.search0103.3.10.13351a97VKZFxl&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10890_319_10546_10059_10884_317_10548_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536_10902,searchweb201603_57,ppcSwitch_0&algo_expid=c9d24dc2-9068-4891-b219-92b11365aeca-1&algo_pvid=c9d24dc2-9068-4891-b219-92b11365aeca&transAbTest=ae803_3',
                [
                    'name' => 'Men\'s T Shirt 2018 Summer Slim Fitness Hooded Short-Sleeved Tees Male Camisa Masculina Sportswer T-Shirt Slim Tshirt Homme 5XL',
                    'original_price' => '17.98',
                    'sale_price' => '9.17',
                    'categories_count' => 3,
                    'images_count' => 6,
                    'attributes_count' => 2
                ]
            ],
            'aliexpress-2' => [
                'https://www.aliexpress.com/item/Original-Apple-iPhone-X-Face-ID-64GB-256GB-ROM-5-8-inch-3GB-RAM-12MP-Hexa/32842019319.html?spm=2114.search0104.3.7.4d3376c61L4Mp8&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10890_319_10546_10059_10884_317_10548_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536_10902,searchweb201603_57,ppcSwitch_0&algo_expid=235b3cfc-c645-4337-9ee2-001e8f933acb-1&algo_pvid=235b3cfc-c645-4337-9ee2-001e8f933acb&transAbTest=ae803_3',
                [
                    'name' => 'Original Apple iPhone X Face ID 64GB/256GB ROM 5.8 inch 3GB RAM 12MP Hexa Core iOS A11 Dual Back Camera 4G LTE Unlock iphonex',
                    'sale_price' => '925',
                    'original_price' => '1250.00',
                    'categories_count' => 2,
                    'images_count' => 6,
                    'attributes_count' => 2
                ]
            ],
            'aliexpress-3' => [
                'https://www.aliexpress.com/item/Vancat-Winter-Warm-Plush-Fur-Snow-Boots-Men-Ankle-Boot-Quality-Casual-Motorcycle-Boot-Waterproof-Men/32922501594.html?spm=2114.search0103.3.85.6a451edepnPSGX&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10890_319_10546_10059_10884_317_10548_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536_10902,searchweb201603_57,ppcSwitch_0&algo_expid=31e3b601-400a-4efc-ab79-d4ffb99963df-10&algo_pvid=31e3b601-400a-4efc-ab79-d4ffb99963df&transAbTest=ae803_3',
                [
                    'name' => 'Vancat Winter Warm Plush Fur Snow Boots Men Ankle Boot Quality Casual Motorcycle Boot Waterproof  Men\'s Boots Big Size 39-47',
                    'sale_price' => '31.92',
                    'original_price' => '53.20',
                    'categories_count' => 4,
                    'images_count' => 6,
                    'attributes_count' => 2
                ]
            ],
            'aliexpress-mob-1' => [
                'https://m.aliexpress.com/item/32818015370.html?trace=wwwdetail2mobilesitedetail&productId=32818015370&productSubject=WWOOR-Brand-Ladies-Women-Dress-Watches-Thin-Quartz-Watch-Steel-Mesh-Band-Luxury-Casual-Gold-Bracelet&spm=a2g01.11715693.fdmli001.9.44257e25YsKFnn&gps-id=5547572&scm=1007.19201.111800.0&scm_id=1007.19201.111800.0&scm-url=1007.19201.111800.0&pvid=60f7c027-ade8-4164-ab33-a10bca95f552&_t=gps-id:5547572,scm-url:1007.19201.111800.0,pvid:60f7c027-ade8-4164-ab33-a10bca95f552',
                [
                    'name' => 'WWOOR Women Dress Watches Luxury Brand Ladies Quartz Watch Stainless Steel Mesh Band Casual Gold Bracelet Wristwatch reloj mujer',
                    'sale_price' => '159.90',
                    'original_price' => '159.90',
                    'categories_count' => 0,
                    'images_count' => 8,
                    'attributes_count' => 0
                ]
            ],
            'aliexpress-mob-2' => [
                'https://m.aliexpress.com/item/32838188456.html?trace=wwwdetail2mobilesitedetail&productId=32838188456&productSubject=WWOOR-Ladies-Wrist-Watches-for-Women-Ultra-Thin-Quartz-Watch-Fashion-Casual-Hours-Bracelet-Watches-reloj&spm=2114.mbest.2.1.39647423HlCRW0&scm=1007.17258.123347.0&pvid=8da88598-03af-4c09-91b7-ddaa7898a586&aff_platform=msite_best&sk=yjq3vrZ&aff_trace_key=0eb9404e74bd4ac2a6d0c131550db184-1550242944915-05432-yjq3vrZ',
                [
                    'name' => 'WWOOR Ladies Wrist Watches for Women Ultra Thin Quartz Watch Fashion Casual Hours Bracelet Watches reloj mujer acero inoxidable',
                    'sale_price' => '13.96',
                    'original_price' => '29.08',
                    'categories_count' => 0,
                    'images_count' => 8,
                    'attributes_count' => 0
                ]
            ],
            'victoriassecret-1' => [
                'https://ww.victoriassecret.com/lingerie/new-arrivals/logo-lace-cutout-teddy-very-sexy?ProductID=441779&CatalogueType=OLS',
                [
                    'name' => 'Supersoft Cami & Short Set',
                    'description_prefix' => 'Sleep',
                    'description_suffix' => 'nylon',
                    'sale_price' => '49.50',
                    'original_price' => '49.50',
                    'categories_count' => 3,
                    'images_count' => 3,
                    'attributes_count' => 2
                ]
            ],
            'victoriassecret-2' => [
                'https://ww.victoriassecret.com/clearance/sleep/the-sleepover-knit-pj-victorias-secret?ProductID=423771&CatalogueType=OLS',
                [
                    'name' => 'The Sleepover Knit PJ',
                    'description_prefix' => 'A cute',
                    'description_suffix' => 'polyester ',
                    'sale_price' => '19.99',
                    'original_price' => '54.50',
                    'categories_count' => 3,
                    'images_count' => 3,
                    'attributes_count' => 3
                ]
            ],
            'oldnavy-1' => [
                'https://oldnavy.gap.com/browse/product.do?pid=393746012&cid=1060249&pcid=65080',
                [
                    'name' => 'Tie-Front Secret-Slim Underwire Plus-Size Swimsuit',
                    'description_prefix' => 'Adjustable',
                    'description_suffix' => '393746',
                    'original_price' => '54.99',
                    'sale_price' => '53',
                    'categories_count' => 0,
                    'images_count' => 12,
                    'attributes_count' => 3
                ]
            ],
            'oldnavy-2' => [
                'https://oldnavy.gap.com/browse/product.do?pid=347535002&cid=1125675&pcid=1031103',
                [
                    'name' => 'Ultra-Soft Breathe ON  Built-In Flex Color-Blocked Tee for Men',
                    'description_prefix' => 'Food',
                    'description_suffix' => '347535',
                    'original_price' => '22.99',
                    'sale_price' => '20.00',
                    'categories_count' => 0,
                    'images_count' => 6,
                    'attributes_count' => 3
                ]
            ],
            'gap-1' => [
                'https://www.gap.com/browse/product.do?pid=442019002&cid=1068286&pcid=6300',
                [
                    'name' => 'Embroidered Chest-Stripe T-Shirt Dress',
                    'description_prefix' => 'Soft knit',
                    'description_suffix' => '442019',
                    'original_price' => '24.95',
                    'sale_price' => '18.00',
                    'categories_count' => 2,
                    'images_count' => 4,
                    'attributes_count' => 2
                ]
            ],
            'gap-2' => [
                'https://www.gap.com/browse/product.do?pid=440288002&cid=1084650&pcid=11900',
                [
                    'name' => 'Textured Crewneck Pullover Sweater',
                    'description_prefix' => 'Soft',
                    'description_suffix' => '440288',
                    'original_price' => '59.95',
                    'sale_price' => '54',
                    'categories_count' => 2,
                    'images_count' => 5,
                    'attributes_count' => 3
                ]
            ],
            'gap-3' => [
                'https://www.gap.com/browse/product.do?pid=418104002&cid=1082634&pcid=1005439',
                [
                    'name' => 'GapFit High Rise Print Capris in Sculpt Revolution',
                    'description_prefix' => 'The waist',
                    'description_suffix' => '418104',
                    'original_price' => '69.95',
                    'sale_price' => '49.00',
                    'categories_count' => 2,
                    'images_count' => 5,
                    'attributes_count' => 3
                ]
            ],
            '6pm-1' => [
                'https://www.6pm.com/p/under-armour-ua-highlight-ace-midnight-navy-white-white/product/9022416/color/335849',
                [
                    'name' => 'Under Armour UA Highlight Ace',
                    'description_prefix' => 'SKU',
                    'description_suffix' => '4 in',
                    'original_price' => '124.99',
                    'sale_price' => '82.45',
                    'categories_count' => 3,
                    'images_count' => 7,
                    'attributes_count' => 3
                ]
            ],
            '6pm-2' => [
                'https://www.6pm.com/p/tolani-aniya-tunic-dress-blossom/product/9116976/color/18872',
                [
                    'name' => 'Tolani Aniya Tunic Dress',
                    'description_prefix' => 'SKU',
                    'description_suffix' => '36 in',
                    'original_price' => '99.00',
                    'sale_price' => '44.99',
                    'categories_count' => 3,
                    'images_count' => 4,
                    'attributes_count' => 2
                ]
            ],
            '6pm-3' => [
                'https://www.6pm.com/p/michael-kors-mk3795-portia-rose-gold/product/9011286',
                [
                    'name' => 'Michael Kors MK3795 - Portia',
                    'description_prefix' => 'SKU',
                    'description_suffix' => '3 oz',
                    'original_price' => '225',
                    'sale_price' => '157.99',
                    'categories_count' => 3,
                    'images_count' => 3,
                    'attributes_count' => 1
                ]
            ],
            '6pm-4' => [
                'https://www.6pm.com/p/adidas-ultimate-v-neck-short-sleeve-tee-white/product/8851506/color/14',
                [
                    'name' => 'adidas Ultimate V-Neck Short Sleeve Tee',
                    'description_prefix' => 'SKU',
                    'description_suffix' => '29 in',
                    'original_price' => '25',
                    'sale_price' => '19.99',
                    'categories_count' => 3,
                    'images_count' => 4,
                    'attributes_count' => 2
                ]
            ],
            'ebay-1' => [
                'https://www.ebay.com/itm/Imaginext-DC-Justice-League-7pk-Solomon-Grundy-Action-Figure-Fisher-Price-CHOP/232319478210',
                [
                    'name' => 'Imaginext DC Justice League 7pk Solomon Grundy Action Figure Fisher-Price CHOP',
                    'original_price' => '39.99',
                    'sale_price' => '39.99',
                    'categories_count' => 3,
                    'images_count' => 5,
                    'attributes_count' => 0
                ]
            ],
            'ebay-2' => [
                'https://www.ebay.com/itm/Apple-Watch-Series-1-42mm-Space-Gray-Black-Sport-Band-MP032LL-A-A-Grade/132619062663',
                [
                    'name' => 'Apple Watch Series 1 42mm Space Gray Black Sport Band MP032LL/A A Grade',
                    'original_price' => '239.99',
                    'sale_price' => '154.99',
                    'categories_count' => 2,
                    'images_count' => 4,
                    'attributes_count' => 0
                ]
            ],
            'ebay-3' => [
                'https://www.ebay.com/itm/Weed-T-shirt-Ganja-420-THC-Hand-Rap-Style-Short-Slee-Designer-Fashion-Leaf-Weed/143103844278?hash=item2151a75fb6:m:mqzgDQflQRA_JyoSUdC91sQ&var=442061587937',
                [
                    'name' => 'Weed T-shirt Ganja 420 THC Hand Rap Style Short Slee Designer Fashion Leaf Weed',
                    'original_price' => '9.57',
                    'sale_price' => '9.57',
                    'categories_count' => 4,
                    'images_count' => 73,
                    'attributes_count' => 2
                ]
            ],
            'ebay-4' => [
                'https://www.ebay.com/itm/Brand-New-Garmin-Sport-Heart-Rate-Monitor-with-Chest-Strap-HRM-ANT/162802967896?hash=item25e7d02558:g:nXUAAOSw6GJaFzea',
                [
                    'name' => 'Brand New Garmin Sport Heart Rate Monitor with Chest Strap HRM ANT+',
                    'original_price' => '19.59',
                    'sale_price' => '19.59',
                    'categories_count' => 4,
                    'images_count' => 2,
                    'attributes_count' => 0
                ]
            ],
            'forever21-1' => [
                'https://www.forever21.com/us/shop/catalog/product/f21/app-main/2000335385',
                [
                    'name' => 'Long Sleeve Crop Top',
                    'description_prefix' => 'Details',
                    'description_suffix' => 'Small',
                    'original_price' => '12.90',
                    'sale_price' => '12.90',
                    'categories_count' => 2,
                    'images_count' => 4,
                    'attributes_count' => 2
                ]
            ],
            'forever21-2' => [
                'https://www.forever21.com/us/shop/catalog/product/plus/plus_size-dresses/2000332194',
                [
                    'name' => 'Plus Size Sweater Dress',
                    'description_prefix' => 'Details',
                    'description_suffix' => '1X',
                    'original_price' => '17.90',
                    'sale_price' => '17.90',
                    'categories_count' => 1,
                    'images_count' => 4,
                    'attributes_count' => 2
                ]
            ],
            'forever21-3' => [
                'https://www.forever21.com/us/shop/catalog/product/f21/sale/2000277969',
                [
                    'name' => 'Crinkled Smocked Peasant Dress',
                    'description_prefix' => 'Details',
                    'description_suffix' => 'Small',
                    'original_price' => '24.9',
                    'sale_price' => '17.43',
                    'categories_count' => 2,
                    'images_count' => 4,
                    'attributes_count' => 2
                ]
            ],
            'gapfactory-1' => [
                'https://www.gapfactory.com/browse/product.do?cid=1124498&pcid=1041624&vid=1&pid=927279001',
                [
                    'name' => 'Mid Rise Legging Jeans',
                    'description_prefix' => 'Our best',
                    'description_suffix' => '927279',
                    'original_price' => '59.99',
                    'sale_price' => '28.99',
                    'categories_count' => 0,
                    'images_count' => 3,
                    'attributes_count' => 3
                ]
            ],
            'carters-1' => [
                'https://www.carters.com/carters-baby-boy-short-sleeve-multi-packs/V_126H577.html?navid=carters-xsellPDPYMAL2',
                [
                    'name' => '5-Pack Vehicle Original Bodysuits',
                    'description_prefix' => 'Crafted',
                    'description_suffix' => 'outfit!',
                    'original_price' => '26.00',
                    'sale_price' => '13',
                    'categories_count' => 4,
                    'images_count' => 4,
                    'attributes_count' => 2
                ]
            ],
            'sephora-1' => [
                'https://www.sephora.com/product/beach-fragrance-P270559?icid2=products%20grid:p270559:product&skuId=1628940',
                [
                    'name' => 'Beach Fragrance',
                    'description_prefix' => '<strong>What it is',
                    'description_suffix' => 'anywhere.',
                    'original_price' => '28.00',
                    'sale_price' => '19.00',
                    'categories_count' => 3,
                    'images_count' => 1,
                    'attributes_count' => 1
                ]
            ],
            'sephora-2' => [
                'https://www.sephora.com/product/vanish-seamless-finish-liquid-foundation-P440458?icid2=justarrivedmakeup_skugrid_ufe:p440458:product&skuId=2174779',
                [
                    'name' => 'Vanish&trade; Seamless Finish Liquid Foundation',
                    'description_prefix' => '<b>',
                    'description_suffix' => 'imperfections',
                    'original_price' => '56.00',
                    'sale_price' => '56.00',
                    'categories_count' => 3,
                    'images_count' => 1,
                    'attributes_count' => 1
                ]
            ],
            'sephora-3' => [
                'https://www.sephora.com/product/hyaluronic-eye-cream-P440502?icid2=mariolp_fromthebrand_010119_us_carousel:p440502:product',
                [
                    'name' => 'Hyaluronic Eye Cream',
                    'description_prefix' => '<b>',
                    'description_suffix' => 'area.',
                    'original_price' => '18.00',
                    'sale_price' => '18.00',
                    'categories_count' => 3,
                    'images_count' => 1,
                    'attributes_count' => 0
                ]
            ],
            'rue21-1' => [
                'https://www.rue21.com/store/jump/product/Plus-Oatmeal-Off-The-Shoulder-Mini-Dress/7235-001110-0007484-0015',
                [
                    'name' => 'Plus Oatmeal Off The Shoulder Mini Dress',
                    'description_prefix' => 'Fancy',
                    'description_suffix' => 'rue21!',
                    'original_price' => '21.99',
                    'sale_price' => '21.99',
                    'categories_count' => 0,
                    'images_count' => 1,
                    'attributes_count' => 2
                ]
            ],
            'rue21-2' => [
                'https://www.rue21.com/store/jump/product/Black-Metallic-Side-Buckle-Knee-High-Boots---Wide-Width/0372-002175-0008997-0001',
                [
                    'name' => 'Black Metallic Side Buckle Knee High Boots - Wide Width',
                    'description_prefix' => 'These',
                    'description_suffix' => 'rue21!',
                    'original_price' => '49.99',
                    'sale_price' => '12.00',
                    'categories_count' => 0,
                    'images_count' => 1,
                    'attributes_count' => 2
                ]
            ],
            'guessfactory-1' => [
                'https://www.guessfactory.com/en/catalog/view/Q83K00R6BH0/',
                [
                    'name' => 'Nivella Logo Midi Dress',
                    'description_prefix' => 'This',
                    'description_suffix' => 'wash',
                    'original_price' => '44.99',
                    'sale_price' => '31.49',
                    'categories_count' => 3,
                    'images_count' => 4,
                    'attributes_count' => 2
                ]
            ],
            'guessfactory-2' => [
                'https://www.guessfactory.com/en/catalog/view/women/shoes/heels/nancee-gladiator-platform-heels/gfnancee?color=blkll',
                [
                    'name' => 'Nancee Gladiator Platform Heels',
                    'description_prefix' => 'The perfect',
                    'description_suffix' => 'Synthetic',
                    'original_price' => '69.99',
                    'sale_price' => '52.49',
                    'categories_count' => 3,
                    'images_count' => 5,
                    'attributes_count' => 2
                ]
            ],
            'gbyguess-1' => [
                'https://www.gbyguess.com/en/catalog/view/womens/all-apparel/usha-linen-jacket/r91n01r7e02',
                [
                    'name' => 'Usha Linen Jacket',
                    'description_prefix' => 'Lightweight',
                    'description_suffix' => 'wash',
                    'original_price' => '49.99',
                    'sale_price' => '34.99',
                    'categories_count' => 2,
                    'images_count' => 4,
                    'attributes_count' => 2
                ]
            ],
            'gbyguess-2' => [
                'https://www.gbyguess.com/en/catalog/view/womens/accessories/sunglasses/matte-round-sunglasses/gg1178w',
                [
                    'name' => 'Matte Round Sunglasses',
                    'description_prefix' => 'Seal',
                    'description_suffix' => 'warranty',
                    'original_price' => '49.99',
                    'sale_price' => '34.99',
                    'categories_count' => 3,
                    'images_count' => 3,
                    'attributes_count' => 2
                ]
            ],
            'guess-1' => [
                'https://shop.guess.com/en/catalog/view/women/activewear/paxton-biker-shorts/w9fd91r49a5',
                [
                    'name' => 'Paxton Biker Shorts',
                    'description_prefix' => 'A layering',
                    'description_suffix' => 'wash',
                    'original_price' => '29.00',
                    'sale_price' => '29.00',
                    'categories_count' => 2,
                    'images_count' => 5,
                    'attributes_count' => 2
                ]
            ],
            'etsy-1' => [
                'https://www.etsy.com/listing/623021113/bordeaux-leather-sandals-leather-flats?ref=listing-shop-header-2',
                [
                    'name' => 'Bordeaux Leather Sandals, Leather Flats, Red Cutout leather Sandals, Summer Shoes , Free Shipping',
                    'description_prefix' => 'Free',
                    'description_suffix' => '===',
                    'original_price' => '105.00',
                    'sale_price' => '105.00',
                    'categories_count' => 4,
                    'images_count' => 10,
                    'attributes_count' => 2
                ]
            ],
            'etsy-2' => [
                'https://www.etsy.com/listing/210500902/lace-up-brown-leather-ankle-boots-zoey?ref=hp_rv&pro=1&frs=1',
                [
                    'name' => 'Lace Up Brown Leather Ankle boots, Zoey // Free Shipping',
                    'description_prefix' => 'These hiking',
                    'description_suffix' => '38)',
                    'original_price' => '295.00',
                    'sale_price' => '250.75',
                    'categories_count' => 4,
                    'images_count' => 10,
                    'attributes_count' => 1
                ]
            ],
            'ralphlauren-1' => [
                'https://www.ralphlauren.com/men-clothing-polo-shirts/custom-slim-fit-mesh-polo/401481.html',
                [
                    'name' => 'Custom Slim Fit Mesh Polo',
                    'description_prefix' => 'An American',
                    'description_suffix' => '401481',
                    'original_price' => '85.00',
                    'sale_price' => '85',
                    'categories_count' => 3,
                    'images_count' => 5,
                    'attributes_count' => 2
                ]
            ],
            'ralphlauren-2' => [
                'https://www.ralphlauren.com/women/aran-knit-wool-blend-sweater/465014.html?cgid=women&dwvar465014_colorname=Walrus%20Brown%20Heather&webcat=Women#prefn1=SaleFlag&srule=women-sale&prefv1=Sale&ab=en_US_SLP_Slot_1_S1_L2_SHOP&start=1&cgid=women',
                [
                    'name' => 'Aran-Knit Wool-Blend Sweater',
                    'description_prefix' => 'A chunky',
                    'description_suffix' => '465014',
                    'original_price' => '498.00',
                    'sale_price' => '299.99',
                    'categories_count' => 3,
                    'images_count' => 5,
                    'attributes_count' => 2
                ]
            ],
            'urbanog-1' => [
                'https://www.urbanog.com/Pull-On-Elasticized-Ankle-Strap-Stiletto-Heel_181_87873.html',
                [
                    'name' => 'Pull-On Elasticized Ankle Strap Stiletto Heel',
                    'description_prefix' => 'These',
                    'description_suffix' => 'insole.',
                    'original_price' => '52.90',
                    'sale_price' => '23.80',
                    'categories_count' => 0,
                    'images_count' => 3,
                    'attributes_count' => 0
                ]
            ],
            'urbanog-2' => [
                'https://www.urbanog.com/liliana-clear-jewel-embellished-t-strap-stiletto-heel_100_87001.html',
                [
                    'name' => 'Liliana Clear Jewel Embellished T-Strap Stiletto Heel',
                    'description_prefix' => 'These',
                    'description_suffix' => 'insole.',
                    'original_price' => '58.00',
                    'sale_price' => '26.10',
                    'categories_count' => 0,
                    'images_count' => 3,
                    'attributes_count' => 0
                ]
            ],
            'newbalance-1' => [
                'https://www.newbalance.com/pd/mens-fresh-foam-cruz-v2-knit/MCRUZ-V2K.html?dwvar_MCRUZ-V2K_color=White_with_Black#color=White_with_Black',
                [
                    'name' => 'Men\'s Fresh Foam Cruz v2 Knit',
                    'description_prefix' => 'A lightweight',
                    'description_suffix' => 'men.',
                    'original_price' => '84.99',
                    'sale_price' => '59.99',
                    'categories_count' => 4,
                    'images_count' => 6,
                    'attributes_count' => 3
                ]
            ],
            'newbalance-2' => [
                'https://www.newbalance.com/pd/q-speed-softwear-crew/MT91252.html?dwvar_MT91252_color=Heather%20Charcoal#color=Cashmere',
                [
                    'name' => 'Q Speed Softwear Crew',
                    'description_prefix' => 'For early ',
                    'description_suffix' => 'wrists.',
                    'original_price' => '74.99',
                    'sale_price' => '74.99',
                    'categories_count' => 4,
                    'images_count' => 2,
                    'attributes_count' => 2
                ]
            ],
            'shoes-1' => [
                'https://www.shoes.com/puma-uprise-knit-sneaker/847925/1873533',
                [
                    'name' => 'PUMA Uprise Knit Sneaker',
                    'description_prefix' => 'Men\'s PUMA',
                    'description_suffix' => 'silhouette',
                    'original_price' => '80',
                    'sale_price' => '61.95',
                    'categories_count' => 0,
                    'images_count' => 6,
                    'attributes_count' => 3
                ]
            ],
            'shoes-2' => [
                'https://www.shoes.com/mizuno-wave-rider-22-running-shoe/843896/1862378',
                [
                    'name' => 'Mizuno Wave Rider 22 Running Shoe',
                    'description_prefix' => 'Men\'s Mizuno Wave',
                    'description_suffix' => 'running',
                    'original_price' => '119.95',
                    'sale_price' => '119.95',
                    'categories_count' => 0,
                    'images_count' => 6,
                    'attributes_count' => 3
                ]
            ],
            'childrensplace-1' => [
                'https://www.childrensplace.com/us/p/Girls-Flip-Sequin-Hi-Top-Sneakers-2623471-BQ',
                [
                    'name' => 'Girls Flip Sequin Hi Top Sneakers',
                    'description_prefix' => 'Make her steps',
                    'description_suffix' => 'Prices',
                    'original_price' => '39.95',
                    'sale_price' => '23.97',
                    'categories_count' => 0,
                    'images_count' => 3,
                    'attributes_count' => 2
                ]
            ],
            'childrensplace-2' => [
                'https://www.childrensplace.com/us/p/Baby-And-Toddler-Girls-Short-Flutter-Sleeve-Glitter-Graphic-Top-2118477-1707',
                [
                    'name' => 'Baby And Toddler Girls Short Flutter Sleeve Glitter Graphic Top',
                    'description_prefix' => 'A cute and comfy',
                    'description_suffix' => 'Prices',
                    'original_price' => '16.95',
                    'sale_price' => '8.48',
                    'categories_count' => 0,
                    'images_count' => 3,
                    'attributes_count' => 2
                ]
            ],
            'finishline-1' => [
                'https://www.finishline.com/store/product/kids-nike-heritage86-strapback-hat/prod2783376?styleId=AJ3651&colorId=663',
                [
                    'name' => 'Kids\' Nike Heritage86 Strapback Hat',
                    'description_prefix' => 'Cotton',
                    'description_suffix' => 'fit.',
                    'original_price' => '20',
                    'sale_price' => '15',
                    'categories_count' => 1,
                    'images_count' => 2,
                    'attributes_count' => 2
                ]
            ],
            'finishline-2' => [
                'https://www.finishline.com/store/product/mens-jordan-legacy-aj6-t-shirt/prod2785395?styleId=BV5411&colorId=010',
                [
                    'name' => 'Men\'s Jordan Legacy AJ6 T-Shirt',
                    'description_prefix' => 'Soft',
                    'description_suffix' => 'cop.',
                    'original_price' => '40',
                    'sale_price' => '35',
                    'categories_count' => 1,
                    'images_count' => 6,
                    'attributes_count' => 2
                ]
            ],
            'elfcosmetics-1' => [
                'https://www.elfcosmetics.com/16hr-camo-concealer/85841.html?cgid=eyes#start=2',
                [
                    'name' => '16HR Camo Concealer',
                    'description_prefix' => 'A full-coverage',
                    'description_suffix' => 'finish.',
                    'original_price' => '5',
                    'sale_price' => '5',
                    'categories_count' => 2,
                    'images_count' => 8,
                    'attributes_count' => 1
                ]
            ],
            'nyxcosmetics-1' => [
                'https://www.nyxcosmetics.com/sweet-chateau-suede-matte-lip-cream-set/NYX_700.html?cgid=sale',
                [
                    'name' => 'Sweet Château Soft Matte Lip Cream Set',
                    'description_prefix' => 'Your lips',
                    'description_suffix' => 'dreams.',
                    'original_price' => '18',
                    'sale_price' => '12.60',
                    'categories_count' => 2,
                    'images_count' => 3,
                    'attributes_count' => 0
                ]
            ],
            'nyxcosmetics-2' => [
                'https://www.nyxcosmetics.com/soft-matte-lip-cream/NYX_007.html?cgid=liquid-lipstick',
                [
                    'name' => 'Soft Matte Lip Cream',
                    'description_prefix' => 'Now available',
                    'description_suffix' => 'favorite.',
                    'original_price' => '6.50',
                    'sale_price' => '6.50',
                    'categories_count' => 2,
                    'images_count' => 3,
                    'attributes_count' => 1
                ]
            ],
        ];
    }

    protected function setUp()
    {
        $this->scraper = new Scraper(new NullLogger(), 'http://192.168.1.3:4444/wd/hub');
    }
}
