<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Tests\Unit\Parsers;

use ITeam\Ecommerce\Scraper\Parsers\Ebay;

class EbayTest extends TestCase
{
    protected $fixturePath = '/../fixtures/ebay.html';

    public function testGetImages()
    {
        $this->assertCount(9, $this->parser->getImages());
    }

    public function testGetSalePrice()
    {
        $this->assertEquals('23.95', $this->parser->getSalePrice());
    }

    public function testGetCategories()
    {
        $expected = [
            'Sporting Goods',
            'Golf',
            'Golf Clothing, Shoes & Accs',
            'Men\'s Golf Clothing & Shoes',
            'Shirts, Tops & Sweaters',
            'Clothing, Shoes & Accessories',
            'Men\'s Clothing',
            'Coats & Jackets'
        ];


        $this->assertEquals($expected, $this->parser->getCategories());
    }

    public function testGetAttributes()
    {
        $attributes = $this->parser->getAttributes();
        $this->assertCount(2, $attributes);
        $this->assertCount(8, $attributes[0]->getValues());
        $this->assertCount(5, $attributes[1]->getValues());
    }

    public function testGetName()
    {
        $expected = 'Adidas Golf 2017 Competition 1/4 Zip Pullover Layering Top - Pick Color &amp; Size';
        $this->assertEquals($expected, $this->parser->getName());
    }

    public function testGetDescription()
    {
        $this->assertEquals('', $this->parser->getDescription());
    }

    public function testGetOriginalPrice()
    {
        $this->assertEquals('90.00', $this->parser->getOriginalPrice());
    }

    protected function setUp()
    {
        parent::setUp();
        $this->parser = new Ebay($this->crawler);
    }
}
