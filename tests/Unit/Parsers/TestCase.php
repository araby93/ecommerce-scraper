<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Tests\Unit\Parsers;

use ITeam\Ecommerce\Scraper\Parsers\ParserInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class TestCase
 * @package ITeam\Ecommerce\Scraper\Tests\Parsers
 */
class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Crawler
     */
    protected $crawler;
    /**
     * @var string
     */
    protected $fixturePath;
    /**
     * @var ParserInterface
     */
    protected $parser;

    /**
     *
     */
    protected function setUp()
    {
        $html = file_get_contents(__DIR__ . $this->fixturePath);
        $this->crawler = new Crawler($html);
    }
}
