<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Tests\Unit\Parsers;

use ITeam\Ecommerce\Scraper\Parsers\AliExpress;

class AliExpressTest extends TestCase
{
    protected $fixturePath = '/../fixtures/aliexpress.html';

    public function testGetImages()
    {
        $this->assertCount(5, $this->parser->getImages());
    }

    public function testGetSalePrice()
    {
        $this->assertEquals('11.30', $this->parser->getSalePrice());
    }

    public function testGetCategories()
    {
        $expected = [
            'Men\'s Clothing',
            'Tops & Tees',
            'T-Shirts'
        ];


        $this->assertEquals($expected, $this->parser->getCategories());
    }

    public function testGetAttributes()
    {
        $attributes = $this->parser->getAttributes();
        $this->assertCount(3, $attributes);
        $this->assertCount(8, $attributes[0]->getValues());
        $this->assertCount(7, $attributes[1]->getValues());
        $this->assertCount(2, $attributes[2]->getValues());
    }

    public function testGetName()
    {
        $expected = 'Men\'S T Shirt 2018 Summer Fashion Hooded Sling Short-Sleeved Tees Male Camisa Masculina T-Shirt Slim Male Tops 4XL';
        $this->assertEquals($expected, $this->parser->getName());
    }

    public function testGetDescription()
    {
        $this->assertEquals('', $this->parser->getDescription());
    }

    public function testGetOriginalPrice()
    {
        $this->assertEquals('11.30', $this->parser->getOriginalPrice());
    }

    protected function setUp()
    {
        parent::setUp();
        $this->parser = new AliExpress($this->crawler);
    }
}
