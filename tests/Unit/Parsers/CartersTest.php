<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Tests\Unit\Parsers;

use ITeam\Ecommerce\Scraper\Parsers\Carters;

class CartersTest extends TestCase
{
    protected $fixturePath = '/../fixtures/carters.html';

    public function testGetImages()
    {
        $expected = [
            'https://cdn-eu-ec.yottaa.net/578855e22bb0ac10350002d6/www.carters.com/v~4b.3c/dw/image/v2/AAMK_PRD/on/demandware.static/-/Sites-carters_master_catalog/default/dwa6ca9e22/productimages/126H577.jpg',
            'https://cdn-eu-ec.yottaa.net/578855e22bb0ac10350002d6/www.carters.com/v~4b.3c/dw/image/v2/AAMK_PRD/on/demandware.static/-/Sites-carters_master_catalog/default/dwb622a210/productimages/126H577_1.jpg',
            'https://cdn-eu-ec.yottaa.net/578855e22bb0ac10350002d6/www.carters.com/v~4b.3c/dw/image/v2/AAMK_PRD/on/demandware.static/-/Sites-carters_master_catalog/default/dwd3a81202/additional-specs/LBB_FEATURES_BENEFITS_bodysuits.jpg',
            'https://cdn-eu-ec.yottaa.net/578855e22bb0ac10350002d6/www.carters.com/v~4b.3c/dw/image/v2/AAMK_PRD/on/demandware.static/-/Sites-carters_master_catalog/default/dw07f8f0b3/additional-specs/LBB_FEATURES_BENEFITS_size_chart.jpg'
        ];

        $this->assertEquals($expected, $this->parser->getImages());
    }

    public function testGetSalePrice()
    {
        $this->assertEquals('26.00', $this->parser->getSalePrice());
    }

    public function testGetCategories()
    {
        $expected = [
            'Baby Clothes',
            'Baby Boy',
            'Bodysuits',
            'Short-Sleeve Multi-Pack',
        ];


        $this->assertEquals($expected, $this->parser->getCategories());
    }

    public function testGetAttributes()
    {
        $attributes = $this->parser->getAttributes();
        $this->assertCount(2, $attributes);
        $this->assertCount(7, $attributes[0]->getValues());
        $this->assertCount(1, $attributes[1]->getValues());
    }

    public function testGetName()
    {
        $expected = '5-Pack Vehicle Original Bodysuits';
        $this->assertEquals($expected, $this->parser->getName());
    }

    public function testGetDescription()
    {
        $this->assertStringStartsWith('Crafted in soft', $this->parser->getDescription());
        $this->assertStringEndsWith('easy outfit!', $this->parser->getDescription());
    }

    public function testGetOriginalPrice()
    {
        $this->assertEquals('26.00', $this->parser->getOriginalPrice());
    }

    protected function setUp()
    {
        parent::setUp();
        $this->parser = new Carters($this->crawler);
    }
}
