<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Tests\Unit\Parsers;

use ITeam\Ecommerce\Scraper\Parsers\Amazon;

class AmazonTest extends TestCase
{
    protected $fixturePath = '/../fixtures/amazon.html';

    public function testGetImages()
    {
        $expected = [
            'https://images-na.ssl-images-amazon.com/images/I/4173H5Ga0bL.jpg',
            'https://images-na.ssl-images-amazon.com/images/I/412ezHNbXOL.jpg',
            'https://images-na.ssl-images-amazon.com/images/I/31D7OJ3n00L.jpg',
            'https://images-na.ssl-images-amazon.com/images/I/31u-ERouxzL.jpg'
        ];

        $this->assertEquals($expected, $this->parser->getImages());
    }

    public function testGetSalePrice()
    {
        $this->assertEquals('16.46', $this->parser->getSalePrice());
    }

    public function testGetCategories()
    {
        $expected = [
            'Clothing, Shoes & Jewelry',
            'Men',
            'Clothing',
            'Active',
            'Active Hoodies'
        ];

        $this->assertEquals($expected, $this->parser->getCategories());
    }

    public function testGetAttributes()
    {
        $this->assertCount(2, $this->parser->getAttributes());
    }

    public function testGetName()
    {
        $expected = 'Hanes Men\'s Pullover EcoSmart Fleece Hooded Sweatshirt';
        $this->assertEquals($expected, $this->parser->getName());
    }

    public function testGetDescription()
    {
        $this->assertStringStartsWith('About Hanes', $this->parser->getDescription());
        $this->assertStringEndsWith('www.HanesForGood.com', $this->parser->getDescription());
    }

    public function testGetOriginalPrice()
    {
        $this->assertEquals('31.95', $this->parser->getOriginalPrice());
    }

    protected function setUp()
    {
        parent::setUp();
        $this->parser = new Amazon($this->crawler);
    }
}
