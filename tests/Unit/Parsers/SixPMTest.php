<?php
/**
 * @author Ahmed El-Araby <araby2305@gmail.com>
 */

namespace ITeam\Ecommerce\Scraper\Tests\Unit\Parsers;

use ITeam\Ecommerce\Scraper\Parsers\SixPM;

class SixPMTest extends TestCase
{
    protected $fixturePath = '/../fixtures/6pm.html';

    public function testGetImages()
    {
        $this->assertCount(6, $this->parser->getImages());
    }

    public function testGetSalePrice()
    {
        $this->assertEquals('21', $this->parser->getSalePrice());
    }

    public function testGetCategories()
    {
        $expected = [
            'Clothing',
            'Sweaters',
            'J.O.A.'
        ];


        $this->assertEquals($expected, $this->parser->getCategories());
    }

    public function testGetAttributes()
    {
        $attributes = $this->parser->getAttributes();
        $this->assertCount(2, $attributes);
        $this->assertCount(2, $attributes[0]->getValues());
        $this->assertCount(2, $attributes[1]->getValues());
    }

    public function testGetName()
    {
        $expected = 'J.O.A. Asymmetric Knit Top';
        $this->assertEquals($expected, $this->parser->getName());
    }

    public function testGetDescription()
    {
        $this->assertStringStartsWith('SKU', $this->parser->getDescription());
        $this->assertStringEndsWith('18 in', $this->parser->getDescription());
    }

    public function testGetOriginalPrice()
    {
        $this->assertEquals('70.00', $this->parser->getOriginalPrice());
    }

    protected function setUp()
    {
        parent::setUp();
        $this->parser = new SixPM($this->crawler);
    }
}
